import lsStorage from '../localstore';

export default {
  mixins: [lsStorage],
  data() {
    return {
      cart: [],
    };
  },
  methods: {
    setLocalStorageKey() {
      this.localStorageKey = 'users';
    },
    getCurrentUser() {
      const storage = this.getLocalStorageItem();
      return storage.currentUser;
    },
    getUserCart() {
      const storage = this.getLocalStorageItem();
      const currentUser = this.getCurrentUser();
      const currentUserEmail = currentUser.email;
      const { carts } = this.getLocalStorageItem();
      const newUserCart = {
        email: currentUserEmail,
        list: [],
      };

      if (carts) {
        const userCart = carts.filter(cart => cart.email === currentUserEmail);
        if (userCart.length) {
          return userCart[0];
        }
      }

      if (storage.carts) {
        storage.carts.push(newUserCart);
      } else {
        storage.carts = [newUserCart];
      }
      this.setLocalStorageItem(storage);

      return newUserCart;
    },
    saveUserCart(userCart) {
      const storage = this.getLocalStorageItem();
      const currentUser = this.getCurrentUser();
      const currentUserEmail = currentUser.email;

      storage.carts.forEach((cart) => {
        if (cart.email === currentUserEmail) {
          const currentCart = cart;
          currentCart.list = userCart.list;
        }
      });
      this.setLocalStorageItem(storage);
    },
    addToCart(productName) {
      const userCart = this.getUserCart();
      userCart.list.push({ product: productName, quantity: 1 });
      this.saveUserCart(userCart);
    },
  },
};
