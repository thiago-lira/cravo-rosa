export default {
  data() {
    return {
      localStorageKey: null,
    };
  },
  methods: {
    setLocalStorageKey() {
      throw new Error('You must implement this method in order to use this mixin');
    },
    getLocalStorageItem() {
      const item = localStorage.getItem(this.localStorageKey);
      return JSON.parse(item) || {};
    },
    setLocalStorageItem(value) {
      const stringify = JSON.stringify(value);
      localStorage.setItem(this.localStorageKey, stringify);
    },
  },
  beforeMount() {
    this.setLocalStorageKey();
  },
};
