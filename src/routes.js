import Vue from 'vue';
import Router from 'vue-router';

import Home from './components/pages/Home/index.vue';
import Products from './components/pages/Products/index.vue';
import News from './components/pages/News/index.vue';
import About from './components/pages/About/index.vue';
import Contact from './components/pages/Contact/index.vue';
import User from './components/pages/User/index.vue';

Vue.use(Router);

export default new Router({
  routes: [{
    path: '/',
    name: 'index',
    component: Home,
  },
  {
    path: '/products/:category',
    name: 'products',
    component: Products,
  },
  {
    path: '/novidades',
    name: 'news',
    component: News,
  },
  {
    path: '/sobre-nos',
    name: 'about',
    component: About,
  },
  {
    path: '/atendimento',
    name: 'contact',
    component: Contact,
  },
  {
    path: '/user',
    name: 'user',
    component: User,
  }],
});
